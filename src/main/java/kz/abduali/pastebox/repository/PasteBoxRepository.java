package kz.abduali.pastebox.repository;

import java.util.List;

public interface PasteBoxRepository {
    PasteBoxEntity getByHash(String hash);
    List<PasteBoxEntity> getListOfPublicAndAlice(int amount);
    void add(PasteBoxEntity pasteBoxEntity);
}
