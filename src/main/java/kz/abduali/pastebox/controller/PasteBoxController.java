package kz.abduali.pastebox.controller;

import kz.abduali.pastebox.api.request.PasteBoxRequest;
import kz.abduali.pastebox.api.response.PasteBoxResponse;
import kz.abduali.pastebox.api.response.PasteBoxUrlResponse;
import kz.abduali.pastebox.service.PasteBoxService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;

@RestController
@RequiredArgsConstructor
public class PasteBoxController {
    private final PasteBoxService pasteBoxService;
    @GetMapping("/")
    public Collection<PasteBoxResponse> getPublicPasteBox() {
        return pasteBoxService.getFistPublicPasteBoxes();
    }
    @GetMapping("/{hash}")
    public PasteBoxResponse getByHash(@PathVariable String hash) {

        return pasteBoxService.getByHash(hash);
    }

    @PostMapping("/")
    public PasteBoxUrlResponse add(@RequestBody PasteBoxRequest request) {
        return pasteBoxService.create(request);
    }


}
