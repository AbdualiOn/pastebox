package kz.abduali.pastebox.service;

import kz.abduali.pastebox.api.response.PasteBoxResponse;
import kz.abduali.pastebox.api.request.PasteBoxRequest;
import kz.abduali.pastebox.api.response.PasteBoxUrlResponse;

import java.util.List;

public interface PasteBoxService {
    PasteBoxResponse getByHash(String hash);
    List<PasteBoxResponse> getFistPublicPasteBoxes();
    PasteBoxUrlResponse create(PasteBoxRequest request);
}
