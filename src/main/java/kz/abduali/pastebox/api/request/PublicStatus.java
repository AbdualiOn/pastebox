package kz.abduali.pastebox.api.request;

public enum PublicStatus {
    PUBLIC,
    UNLISTED
}
